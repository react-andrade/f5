import React, { useState } from "react";
import Button from './components/Button';
import Box from './components/Box';
import './assets/styles.css';

function App() {
    let [currBoxArr, setCurrBoxArr] = useState([]);

    function addBox() {
        console.log("Clicked")
        return setCurrBoxArr([...currBoxArr, {
            value: `Box ${currBoxArr.length + 1}`,
            clicked: false
        }]
        )
    }

    function changeColor(key) {
        return setCurrBoxArr(currBoxArr.map((box, i) => i === key - 1 ? { ...box, clicked: !box.clicked } : { ...box }))
    }

    console.log(currBoxArr);

    let boxes = currBoxArr.map((box, i) => <Box key={i} box={box} id={i + 1} handleClick={changeColor} />);

    return (
        <main>
            <Button key={1} handleClick={addBox} />
            <div>
                {boxes}
            </div>
        </main>
    )
}

export default App;