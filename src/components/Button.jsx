import React from 'react';

function Button(props) {

    function addBox() {
        return props.handleClick();
    }

    return (
        <button onClick={addBox}>
            Press this button to add box
        </button>
    )
}

export default Button;