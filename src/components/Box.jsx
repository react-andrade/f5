import React from 'react';

function Box(props) {
    let bg = props.box.clicked ? "#222222" : "#ffffff";
    let color = props.box.clicked ? "#ffffff" : "#222222";

    let styles = {
        backgroundColor: bg,
        color: color
    }

    return (
        <>
            <p style={styles} onClick={() => props.handleClick(props.id)}>{props.box.value}</p>
        </>
    )
}
export default Box;